import os
import torch
import numpy as np
from pathlib import Path

import torch
import torch.distributed as dist


def synchronize_pycocotools_metrics(tensor: torch.Tensor = None, is_distributed: bool = False) -> torch.Tensor:
    """ Synchronize the metrics stats from pycocotools between the different processes.
    N = number of processes
    C = number of categories
    :param tensor: metrics stats from pycocotools dimensions = [N, 12] or [N, 12, C]
    :param is_distributed: True if more than one gpu is used, otherwise False
    :return: a tensor with an average value between process
    """
    # If more than one gpu is used
    if is_distributed:
        # For average metric
        if len(tensor.size()) == 2:
            # Initialize the average tensor
            average = torch.ones(12) * -1
            # Transpose the tensor dimension = [12, num_process]
            tensor = tensor.transpose(0, 1)
            # get the index where metrics value is -1
            minus_one_index = torch.eq(tensor, -1)

            for k in range(12):
                stats = tensor[k, ~minus_one_index[k]]
                if len(stats) != 0:
                    average[k] = torch.mean(stats)

            return average

        # For metric per categories
        else:
            # Initialize the category average tensor
            num_process, _, num_categories = tensor.size()
            category_average = torch.ones((12, num_categories)) * -1
            # transform the result tensor from [N, 12, C] to [12, C, N]
            tensor = torch.hstack(
                [torch.hstack([tensor[k][:, i].unsqueeze(0).transpose(0, 1) for k in range(num_process)])
                 for i in range(num_categories)]).view(12, num_categories, num_process)
            # get the index where metrics value is -1
            minus_one_index = torch.eq(tensor, -1)

            for k in range(12):
                for c in range(num_categories):
                    stats = tensor[k, c, ~minus_one_index[k, c]]
                    if len(stats) != 0:
                        category_average[k, c] = torch.mean(stats)

            return category_average

    # if only one gpu is used
    else:
        return tensor


def convert_coco_bboxes_to_tensor(bboxes: np.array) -> torch.Tensor:
    """ transform bounding boxes from [x, y, w, h] to [x1, y1, x2, y2]

    :param bboxes: bounding boxes
    :return: the bounding boxes in the format [x1, y1, x2, y2]
    """
    if len(bboxes.shape) == 1:
        bboxes[2] = bboxes[0] + bboxes[2]
        bboxes[3] = bboxes[1] + bboxes[3]

        return torch.from_numpy(bboxes)

    bboxes[:, 2] = bboxes[:, 0] + bboxes[:, 2]
    bboxes[:, 3] = bboxes[:, 1] + bboxes[:, 3]

    return torch.from_numpy(bboxes).type(torch.FloatTensor)


def convert_to_xywh(bboxes: torch.Tensor) -> torch.Tensor:
    """ transform bounding boxes from [x1, y1, x2, y2] to [x, y, w, h]

    :param bboxes: a pytorch tensor of bounding boxes
    :return: the converted tensor
    """
    xmin, ymin, xmax, ymax = bboxes.unbind(1)
    return torch.stack((xmin, ymin, xmax - xmin, ymax - ymin), dim=1)


def check_log_exist(path_to_log: str, name_log: str) -> str:
    """Check if a log already exist with this name, if true add an index to the name"""
    # verify the name of the log version
    if os.path.exists(os.path.join(path_to_log, name_log)):
        # look for other version of this log
        root = Path(path_to_log)
        num_version = len([1 for _ in root.glob(f"{name_log}*")])

        # verify if other sample of this version exist
        if num_version == 1:
            name_log += "_2"
        else:
            name_log += f"_{num_version + 1}"

    return name_log


def is_dist_avail_and_initialized():
    if not dist.is_available():
        return False
    if not dist.is_initialized():
        return False
    return True


def get_world_size():
    if not is_dist_avail_and_initialized():
        return 1
    return dist.get_world_size()


def all_gather(data):
    """
    Run all_gather on arbitrary picklable data (not necessarily tensors)
    Args:
        data: any picklable object
    Returns:
        list[data]: list of data gathered from each rank
    """
    world_size = get_world_size()
    if world_size == 1:
        return [data]
    data_list = [None] * world_size
    dist.all_gather_object(data_list, data)
    return data_list