# import necessary packages
import torch
import pytorch_lightning as pl

from typing import *
from transformers import AutoImageProcessor
# from pycocotools.coco import COCO
from torchmetrics.detection.mean_ap import MeanAveragePrecision

from candleocr.utils.utils import convert_to_xywh


class DETRObjectDetector(pl.LightningModule):

    def __init__(self,
                 processor: AutoImageProcessor,
                 model: torch.nn.Module,
                 learning_rate: float = 0.001,
                 weight_decay: float = 0.001,
                 nb_epochs: int = 50000,
                 num_gpus: int = 1,
                ):
        """

        :param datamodule: lightning data module
        :param model:
        :param learning_rate:
        :param weight_decay: weight decay for the optimizer
        :param nb_epochs:
        :param num_gpus:
        """
        super().__init__()

        self.image_processor = processor
        self.num_gpus = num_gpus
        self.is_distributed = True if self.num_gpus > 1 else False
        self.nb_epochs = nb_epochs
        self.learning_rate = learning_rate
        self.weight_dacay = weight_decay
        self.model = model

        self.metric = MeanAveragePrecision(box_format="xywh")
        # self.val_coco_evaluator = CocoEvaluator(coco_gt=coco_val, iou_types=["bbox"])

    def forward(self, x):

        return self.model(x)

    def training_step(self, batch, batch_idx):
        input_data, _ = batch
        pixel_values = input_data["pixel_values"]
        pixel_mask = input_data["pixel_mask"]
        labels = [{k: v.to(self.device) for k, v in t.items()} for t in input_data["labels"]]

        outputs = self.model(pixel_values=pixel_values, pixel_mask=pixel_mask, labels=labels)

        loss = outputs.loss
        loss_dict = outputs.loss_dict

        # log the different loss values in tensorboard
        for k, v in loss_dict.items():
            self.log("TRAINING_LOSSES/" + k, v, sync_dist=True)
        self.log("TRAINING_GLOBAL_LOSS", loss, sync_dist=True)

        return loss

    def validation_step(self, batch, batch_idx):
        # metrics.coco_validation_step(self, batch, batch_idx)

        input_data, coco_annotations = batch
        pixel_values = input_data["pixel_values"]
        pixel_mask = input_data["pixel_mask"]
        labels = [{k: v.to(self.device) for k, v in t.items()} for t in input_data["labels"]]

        outputs = self.model(pixel_values=pixel_values, pixel_mask=pixel_mask, labels=labels)

        loss = outputs.loss
        loss_dict = outputs.loss_dict

        # log the different loss values in tensorboard
        for k, v in loss_dict.items():
            self.log("VALIDATION_LOSSES/" + k, v, sync_dist=True)
        self.log("VALIDATION_GLOBAL_LOSS", loss, sync_dist=True)

        orig_target_sizes = torch.stack([target["orig_size"] for target in input_data["labels"]], dim=0)
        results = self.image_processor.post_process(outputs, target_sizes=orig_target_sizes)

        # provide to metric
        # metric expects a list of dictionaries, each item 
        # containing image_id, category_id, bbox and score keys 
        # predictions = {target['image_id'].item(): output for target, output in zip(labels, results)}
        # print(f"{predictions=}")
        # predictions = self.val_coco_evaluator.prepare_for_coco_detection(predictions)
        # print(f"{predictions=}")
        # self.val_coco_evaluator.update(predictions)

        for pred in results:
            pred["boxes"] = convert_to_xywh(pred["boxes"])
        self.metric.update(preds=results, target=coco_annotations)

    def on_validation_epoch_end(self) -> None:
        # self.val_coco_evaluator.synchronize_between_processes()
        # self.val_coco_evaluator.accumulate()
        # self.val_coco_evaluator.summarize()

        # Log the metrics in Tensorboard
        for k, v in self.metric.compute().items():
            self.log("VALIDATION/" + k, v)

    def configure_optimizers(self):
        parameters = list(self.parameters())
        trainable_parameters = list(filter(lambda p: p.requires_grad, parameters))

        optimizer = torch.optim.AdamW(params=trainable_parameters,
                                      lr=self.learning_rate,
                                      weight_decay=self.weight_dacay)

        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer=optimizer, T_max=self.nb_epochs)

        return [optimizer] , [lr_scheduler]
