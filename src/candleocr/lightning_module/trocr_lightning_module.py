from typing import Dict, List, Optional, Tuple

import torch
import pytorch_lightning as pl
from transformers import TrOCRProcessor
from torchmetrics import CharErrorRate



class TrOCRlightningModule(pl.LightningModule):
    def __init__(
        self,
        model: torch.nn.Module,
        processor: TrOCRProcessor,
        learning_rate: float = 0.001,
        weight_decay: float = 0.001,
        batch_size: int = 1,
        nb_epochs: int = 10,
    ):
        """Lightning module to train/validate/test LayoutLMv2 and LayoutLMv3

        Args:
            model (torch.nn.Module): LayoutLMv2 model
            learning_rate (float, optional): learning rate for the optimizer.
            weight_decay (float, optional): weight decay for AdamW optimizer.
            batch_size (int, optional): batch size to log correctly the metrics in Tensorboard.
            nb_epochs (int, optional): number of epochs is used for the CosineAnnealingLR learning rate scheduler.
            score_threshold (float, optional): threshold score to compute the metrics. Defaults to 0.5.
        """
        super(TrOCRlightningModule, self).__init__()
        self.model = model
        self.processor = processor
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.batch_size = batch_size
        self.nb_epochs = nb_epochs
        self.cer = CharErrorRate()


    def _run_step_evaluation(self, batch: List[torch.Tensor], prefix_dataset: str):
        """Define the logic of step Validation or Test dataset.

        Args:
            batch (Tuple[List[torch.Tensor]]): batch of data
            prefix_dataset (str, optional): prefix used to log results corresponding to the right dataset.
        """

        label_ids = batch["labels"]
        
        outputs = self.model.generate(batch["pixel_values"])
        pred_str = self.processor.batch_decode(outputs, skip_special_tokens=True)
        label_ids[label_ids == -100] = self.processor.tokenizer.pad_token_id
        label_str = self.processor.batch_decode(label_ids, skip_special_tokens=True)

        # print(f"{pred_str=}")
        # print(f"{label_str=}")

        self.cer.update(preds=pred_str, target=label_str)
        # loss = outputs.loss

        # self.log(f"{prefix_dataset}-loss", loss, batch_size=self.batch_size, sync_dist=True)

    
    # def _run_epoch_end_evaluation(self, prefix_dataset: str):
    #     """Define the logic of end Validation or Test dataset.

    #     Args:
    #         prefix_dataset (str, optional): _description_.
    #     """

    def training_step(self, batch, batch_idx):

        outputs = self.model(**batch)
        loss = outputs.loss

        self.log("TRAINING-loss", loss, batch_size=self.batch_size, sync_dist=True)
        return loss

    def validation_step(self, batch, batch_idx):
        self._run_step_evaluation(batch, prefix_dataset="VALIDATION")

    def on_validation_epoch_end(self):
        result = self.cer.compute()
        self.log("CER", result)
    #     self._run_epoch_end_evaluation(prefix_dataset="VALIDATION")

    def test_step(self, batch, batch_idx):
        self._run_step_evaluation(batch, prefix_dataset="TEST")

    # def test_epoch_end(self, outputs):
    #     self._run_epoch_end_evaluation(prefix_dataset="TEST")

    def configure_optimizers(self):
        parameters = list(self.parameters())
        trainable_parameters = list(filter(lambda p: p.requires_grad, parameters))
        optimizer = torch.optim.AdamW(
            params=trainable_parameters, lr=self.learning_rate, weight_decay=self.weight_decay\
        )

        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer=optimizer, T_max=self.nb_epochs)

        return [optimizer], [lr_scheduler]
