# import necessary packages
from typing import Tuple
import os
import torch
from PIL import Image
import numpy as np
import albumentations as A
from transformers import AutoImageProcessor

from torch.utils.data import Dataset
from pycocotools.coco import COCO


class CocoDataset(Dataset):
    """
    Load images and a MS COCO json file link to the images, in order to be used
    by Pytorch
    """

    def __init__(self, 
                 processor: AutoImageProcessor, 
                 path_images_dir: str = None, 
                 path_json: str = None, 
                 transform: A.Compose = None):
        """Initialize the CocoDataset

        :param str path_images_dir: path to the images directory
        :param str path_json: path to the corresponding json file
        :param transform: a list of Transform object
        """
        self.processor = processor
        self.path_images_dir = path_images_dir
        self.path_json = path_json
        self.transform = transform
        self.coco = COCO(annotation_file=self.path_json)
        self.image_ids = self.coco.getImgIds()

        self.coco_labels = {}
        self.coco_labels_inverse = {}
        self.classes = {}
        self.labels = {}
        self.load_classes()

    def load_classes(self):
        """Load the classes and create several dictionaries to save the information.

        Dictionaries configuration:
            coco_labels: {key: index_coco_label}
            coco_labels_inverse: {index_coco_label: key}
            classes: {label_name: key}
            labels: {key: label_name}

        Example:

        .. code-block:: python

            coco_labels: {0: 1, 1: 2}
            coco_labels_inverse: {1: 0, 2: 1}
            classes: {'paren_homo_hype': 0, 'duct_wirs_std': 1}
            labels: {0: 'paren_homo_hype', 1: 'duct_wirs_std'}

        """
        # Get categories:
        categories = self.coco.loadCats(self.coco.getCatIds())
        # Make sure the labels are in order
        categories.sort(key=lambda x: x["id"])

        for c in categories:
            self.coco_labels[len(self.classes)] = c["id"]
            self.coco_labels_inverse[c["id"]] = len(self.classes)
            self.classes[c["name"]] = len(self.classes)

        for name_label, key in self.classes.items():
            self.labels[key] = name_label

    def coco_label_to_label(self, label):
        return self.coco_labels_inverse[label]

    def label_to_coco_label(self, label):
        return self.coco_labels[label]

    def load_image(self, image_index: int = None) -> np.ndarray:
        """load an image

        :param image_index:
        :return: the normalized ([0,1]) image corresponding to the index provided
        """
        # Get information on the image in order to get its name
        image_info = self.coco.loadImgs(self.image_ids[image_index])[0]
        # Get full path to the image
        path_image = os.path.join(self.path_images_dir, image_info['file_name'])
        image = Image.open(path_image).convert("RGB")

        return np.array(image)

    def image_aspect_ratio(self, image_index: int = None) -> float:
        """Compute the aspect ratio of an image

        :param image_index:
        :return: the ratio of the width on the height
        """
        image_info = self.coco.loadImgs(self.image_ids[image_index])[0]

        return float(image_info["width"]) / float(image_info["height"])

    def load_annotations(self, image_index: int = None) -> np.ndarray:
        """Load the bounding boxes coordinates and labels for an image.

        :param image_index: the index of the image
        :return: a ndarray with the following size [number_annotations, 5]. It contains the bounding
                 boxes coordinates and labels id
        """
        # Get the annotations id from an image
        annotations_id = self.coco.getAnnIds(imgIds=self.image_ids[image_index], iscrowd=False)
        coco_annotations = self.coco.loadAnns(annotations_id)
        
        return coco_annotations

    def num_classes(self) -> int:
        """
        :return: the number of classes
        """
        return len(self.classes)

    def __len__(self):
        """
        :return: the number of images contains in the dataset
        """
        return len(self.image_ids)

    def __getitem__(self, idx: int = None) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        """Gather an image and its annotations in a single dictionary

        :param idx: index of an image
        :return: a dict containing an image and the corresponding annotations
        """
        image = self.load_image(idx)
        coco_annotations = self.load_annotations(idx)

        if self.transform:
            boxes = []
            category_ids = []
            for annotation in coco_annotations:
                boxes.append(annotation["bbox"])
                category_ids.append(annotation["category_id"])

            transformed = self.transform(image=image, bboxes=np.array(boxes), category_ids=category_ids)
            image = transformed["image"]
            boxes = transformed["bboxes"]
            category_ids = transformed["category_ids"]

            new_coco_annotations = []
            for bbox, category_id, anno in zip(boxes, category_ids, coco_annotations):
                new_coco_annotations.append({"bbox": bbox, "category_id": category_id, "area": anno["area"]})

        else:
            new_coco_annotations = coco_annotations

        target = {'image_id': self.image_ids[idx], 'annotations': new_coco_annotations}
        encoding = self.processor(images=image, annotations=target, return_tensors="pt")
        pixel_values = encoding["pixel_values"].squeeze() # remove batch dimension
        target = encoding["labels"][0] # remove batch dimension

        return pixel_values, target, new_coco_annotations


if __name__ == "__main__":
    image_processor = AutoImageProcessor.from_pretrained("microsoft/table-transformer-detection")

    transform =  A.Compose(
        [
            A.HorizontalFlip(p=1),
            A.VerticalFlip(p=1),
            A.RandomRotate90(p=0),
            A.OneOf(
                [
                    A.ToSepia(),
                    A.Sharpen(),
                    A.RandomBrightnessContrast(),
                    A.ColorJitter(brightness=0.5, hue=0.5),
                ],
                p=0.20,
            ),

        ],
        bbox_params=A.BboxParams(format="coco", min_visibility=1, label_fields=["category_ids"]),
    )

    val_dataset = CocoDataset(
        processor=image_processor,
        path_images_dir=r"D:\PycharmProjects\tableman\table_detector\dataset",
        path_json=r"D:\PycharmProjects\tableman\table_detector\dataset\train_annotations.json",
        transform=transform
    )

    for k in range(len(val_dataset)):
        image, target, coco_anno = val_dataset.__getitem__(k)
        # print(image.size())
        print(k, coco_anno)

    for k in range(len(val_dataset)):
        image, target, coco_anno = val_dataset.__getitem__(k)
        # print(image.size())
        print(k, coco_anno)
    # print(target)

    # image, target = val_dataset.__getitem__(10)

    # print(image.size())
    # print(target)