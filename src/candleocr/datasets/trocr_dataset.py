import os
import torch
import pandas as pd
from torch.utils.data import Dataset
from transformers import TrOCRProcessor
from PIL import Image


class TrOCRDataset(Dataset):
    def __init__(self, root_dir: str, processor: TrOCRProcessor, path_data: str, max_target_length: int=32):
        self.root_dir = root_dir
        self.df = pd.read_pickle(path_data)
        self.processor = processor
        self.max_target_length = max_target_length

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx: int):
        # Get target text 
        target_text = self.df['label'].iloc[idx]

        # Load image
        path_image = os.path.join(self.root_dir, self.df["path"].iloc[idx])
        image = Image.open(path_image).convert("RGB")

        # Prepare image 
        pixel_values = self.processor(image, return_tensors="pt").pixel_values

        # add labels (input_ids) by encoding the text
        labels = self.processor.tokenizer(target_text, 
                                          padding="max_length", 
                                          max_length=self.max_target_length).input_ids
        
        # important: make sure that PAD tokens are ignored by the loss function
        labels = [label if label != self.processor.tokenizer.pad_token_id else -100 for label in labels]

        encoding = {"pixel_values": pixel_values.squeeze(), "labels": torch.tensor(labels)}
        return encoding
    

if __name__ == "__main__":
    trocr_dataset = TrOCRDataset(
        root_dir="D:\PycharmProjects\candle\candleocr\data\words_cropped",
        processor=TrOCRProcessor.from_pretrained("microsoft/trocr-base-stage1"),
        path_data="D:\PycharmProjects\candle\candleocr\data\dataset_trocr.pkl"
    )

    encoding = trocr_dataset.__getitem__(0)

    print(encoding["pixel_values"].size())