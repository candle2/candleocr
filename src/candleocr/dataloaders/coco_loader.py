from typing import List

import torch
import albumentations as A
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, Dataset
from transformers import AutoImageProcessor

from candleocr.datasets import CocoDataset




class CocoDataModule(LightningDataModule):
    """Data module to load the data for training, validation and prediction pipelines.

    To use this module, you need:

    - a directory containing the images for training and validation (both in the same directory)
    - a json file containing the training annotations (COCO format)
    - a json file containing the validation annotations (COCO format)
    """

    def __init__(
        self,
        processor: AutoImageProcessor,
        train_dataset: CocoDataset = None,
        val_dataset: CocoDataset = None,
        test_dataset: CocoDataset = None,
        num_workers: int = 0,
        batch_size: int = 2,
        pin_memory: bool = False,
        drop_last: bool = False,
    ) -> None:
        """

        :param str data_dir: path to where the data are saved
        :param str path_json_train: path to the training annotation file (in COCO format)
        :param str path_json_val: path to the validation annotation file (in COCO format)
        :param int num_workers: how many workers to use for loading data
        :param bool normalize: if true applies image normalization
        :param int batch_size: how many samples per batch to load
        :param bool pin_memory: if true, the data loader will copy Tensors into CUDA pinned memory before returning them
        :param bool drop_last: if true, drops the last incomplete batch
        """

        super().__init__()
        self.processor = processor
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.test_dataset = test_dataset

        self.num_workers = num_workers
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.drop_last = drop_last

        if self.train_dataset:
            self.num_classes = self.train_dataset.num_classes()
            self.labels = self.train_dataset.labels.values()

        if self.test_dataset:
            self.num_classes = self.test_dataset.num_classes()
            self.labels = self.test_dataset.labels.values()


    def train_dataloader(self) -> DataLoader:
        """The Train dataloader"""
        return self._data_loader(self.train_dataset, shuffle=True)

    def val_dataloader(self) -> DataLoader:
        """The validation dataloader"""
        return self._data_loader(self.val_dataset, shuffle=False)

    def test_dataloader(self) -> DataLoader:
        """The validation dataloader"""
        return self._data_loader(self.test_dataset, shuffle=False)

    def _collate_fn(self, batch: List[torch.Tensor]) -> tuple:
        pixel_values = [item[0] for item in batch]
        encoding = self.processor.pad(pixel_values, return_tensors="pt")
        labels = [item[1] for item in batch]
        input_data = {}
        input_data['pixel_values'] = encoding['pixel_values']
        input_data['pixel_mask'] = encoding['pixel_mask']
        input_data['labels'] = labels

        coco_annotations = []
        for item in batch:
            coco_boxes = []
            coco_labels = []
            for anno in item[2]:
                coco_boxes.append(anno["bbox"])
                coco_labels.append(anno["category_id"])
            
            coco_boxes = torch.tensor(coco_boxes)
            coco_labels = torch.tensor(coco_labels)
            coco_annotations.append({"boxes": coco_boxes, "labels": coco_labels})


        return input_data, coco_annotations

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        dataLoader = DataLoader(
            dataset=dataset,
            batch_size=self.batch_size,
            shuffle=shuffle,
            num_workers=self.num_workers,
            drop_last=self.drop_last,
            pin_memory=self.pin_memory,
            collate_fn=self._collate_fn,
        )

        return dataLoader


if __name__ == "__main__":

    from transformers import TableTransformerForObjectDetection
    from torchmetrics.detection.mean_ap import MeanAveragePrecision
    from candleocr.utils.utils import convert_to_xywh

    image_processor = AutoImageProcessor.from_pretrained("microsoft/table-transformer-detection")

    transform =  A.Compose(
        [
            A.HorizontalFlip(p=1),
            A.VerticalFlip(p=0),
            A.RandomRotate90(p=0),
            A.OneOf(
                [
                    A.ToSepia(),
                    A.Sharpen(),
                    A.RandomBrightnessContrast(),
                    A.ColorJitter(brightness=0.5, hue=0.5),
                ],
                p=0.20,
            ),

        ],
        bbox_params=A.BboxParams(format="coco", min_visibility=1, label_fields=["category_ids"]),
    )

    train_dataset = CocoDataset(
        processor=image_processor,
        path_images_dir=r"D:\PycharmProjects\tableman\table_detector\dataset",
        path_json=r"D:\PycharmProjects\tableman\table_detector\dataset\train_annotations.json",
        transform=None
    )

    datamodule = CocoDataModule(
        processor=image_processor,
        train_dataset=train_dataset,
        batch_size=1
    )

    batch, coco_anno = next(iter(datamodule.train_dataloader()))

    model = TableTransformerForObjectDetection.from_pretrained("microsoft/table-transformer-detection", 
                                                               id2label={0: "Table"},
                                                               ignore_mismatched_sizes=True)
    
    checkpoint = r"D:\PycharmProjects\candle\candleocr\checkpoints\table-detector_test_2_epoch=7.ckpt"
    state_dict = torch.load(checkpoint)['state_dict']
    try:
        model.load_state_dict(state_dict, strict=True)
    except RuntimeError:
        for key in list(state_dict.keys()):
            # TODO find a way to generalized the change of layers name (or add new argument)
            # state_dict[key.replace('model.feature_extractor', 'backbone')] = state_dict.pop(key)
            if key in ["model.class_labels_classifier.weight", "model.class_labels_classifier.bias", "model.bbox_predictor.layers.0.weight", "model.bbox_predictor.layers.0.bias",
                        "model.bbox_predictor.layers.1.weight", "model.bbox_predictor.layers.1.bias", "model.bbox_predictor.layers.2.weight", "model.bbox_predictor.layers.2.bias"]:
                state_dict[key.replace("model.", "")] = state_dict.pop(key)
            else:
                state_dict[key.replace("model.model.", "model.")] = state_dict.pop(key)
    model.load_state_dict(state_dict, strict=True)

    outputs = model(**batch)
    # print(f"{outputs=}")
    

    # turn into a list of dictionaries (one item for each example in the batch)
    orig_target_sizes = torch.stack([target["orig_size"] for target in batch["labels"]], dim=0)
    results = image_processor.post_process(outputs, target_sizes=orig_target_sizes)
    for pred in results:
        pred["boxes"] = convert_to_xywh(pred["boxes"])
    # print(f"{outputs=}")
    print(f"{results=}")
    print(f"{batch['labels']=}")
    print(f"{coco_anno=}")

    metric = MeanAveragePrecision(box_format="xywh")

    metric.update(preds=results, target=coco_anno)
    output_metric = metric.compute()
    for k, v in output_metric.items():
        print(k, v)

    # batch_labels_processed = image_processor.post_process_object_detection(batch['labels'], target_sizes=orig_target_sizes)
    # print(f"{batch_labels_processed=}")