from typing import List

import torch
from transformers import TrOCRProcessor
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, Dataset

from candleocr.datasets import TrOCRDataset


class TrOCRDataModule(LightningDataModule):
   
    def __init__(
        self,
        train_dataset: TrOCRDataset = None,
        val_dataset: TrOCRDataset = None,
        test_dataset: TrOCRDataset = None,
        num_workers: int = 0,
        batch_size: int = 2,
        pin_memory: bool = False,
        drop_last: bool = False,
    ) -> None:
        """
        :param int num_workers: how many workers to use for loading data
        :param int batch_size: how many samples per batch to load
        :param bool pin_memory: if true, the data loader will copy Tensors into CUDA pinned memory before returning them
        :param bool drop_last: if true, drops the last incomplete batch
        """

        super().__init__()

        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.test_dataset = test_dataset

        self.num_workers = num_workers
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.drop_last = drop_last


    def train_dataloader(self) -> DataLoader:
        """The Train dataloader"""
        return self._data_loader(self.train_dataset, shuffle=True)

    def val_dataloader(self) -> DataLoader:
        """The validation dataloader"""
        return self._data_loader(self.val_dataset, shuffle=False)

    def test_dataloader(self) -> DataLoader:
        """The validation dataloader"""
        return self._data_loader(self.test_dataset, shuffle=False)

    @staticmethod
    def _collate_fn(batch: List[torch.Tensor]) -> tuple:

        list_pixel_values = [b["pixel_values"] for b in batch]
        tensor_pixel_values = torch.stack(list_pixel_values)
        list_labels = [b["labels"] for b in batch]
        tensor_labels = torch.stack(list_labels)

        return {"pixel_values": tensor_pixel_values, "labels": tensor_labels}

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        dataLoader = DataLoader(
            dataset=dataset,
            batch_size=self.batch_size,
            shuffle=shuffle,
            num_workers=self.num_workers,
            drop_last=self.drop_last,
            pin_memory=self.pin_memory,
            collate_fn=self._collate_fn,
        )

        return dataLoader


if __name__ == "__main__":

    trocr_dataset = TrOCRDataset(
        root_dir="D:\PycharmProjects\candle\candleocr\data\words_cropped",
        processor=TrOCRProcessor.from_pretrained("microsoft/trocr-base-stage1"),
        path_data="D:\PycharmProjects\candle\candleocr\data\dataset_trocr.pkl"
    )

    loader = TrOCRDataModule(train_dataset=trocr_dataset)

    train_loader = loader.train_dataloader()

    encoding = next(iter(train_loader))

    print(f"{encoding['pixel_values'].size()=}")
    print(f"{encoding['labels'].size()=}")


