from typing import Dict, Any

import yaml
import os
import albumentations as A
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from transformers import AutoImageProcessor, TableTransformerForObjectDetection

from candleocr.utils.utils import check_log_exist
from candleocr.datasets import CocoDataset
from candleocr.dataloaders import CocoDataModule
from candleocr.lightning_module import DETRObjectDetector

import torch
torch.cuda.empty_cache()


def train_trocr(cfg: Dict[str, Any]):
    
    image_processor = AutoImageProcessor.from_pretrained("microsoft/table-transformer-detection")

    train_transform =  A.Compose(
        [
            A.HorizontalFlip(p=0.25),
            A.VerticalFlip(p=0.25),
            A.RandomRotate90(p=0.25),
            A.OneOf(
                [
                    A.ToSepia(),
                    A.Sharpen(),
                    A.RandomBrightnessContrast(),
                    A.ColorJitter(brightness=0.5, hue=0.5),
                ],
                p=0.20,
            ),

        ],
        bbox_params=A.BboxParams(format="coco", min_visibility=1, label_fields=["category_ids"]),
    )
  
    train_dataset = CocoDataset(
        processor=image_processor,
        path_images_dir=r"D:\PycharmProjects\tableman\table_detector\dataset",
        path_json=r"D:\PycharmProjects\tableman\table_detector\dataset\train_annotations.json",
        transform=train_transform
    )
    val_dataset = CocoDataset(
        processor=image_processor,
        path_images_dir=r"D:\PycharmProjects\tableman\table_detector\dataset",
        path_json=r"D:\PycharmProjects\tableman\table_detector\dataset\val_annotations.json",
        transform=None
    )

    # Data loader
    datamodule = CocoDataModule(
        processor=image_processor,
        train_dataset=train_dataset,
        val_dataset=val_dataset,
        num_workers=cfg["num_workers"],
        batch_size=cfg["batch_size"],
        drop_last=False,
    )

    #Load Model
    model = TableTransformerForObjectDetection.from_pretrained("microsoft/table-transformer-detection", 
                                                               id2label={0: "Table"},
                                                               ignore_mismatched_sizes=True)

    name_logger_version = check_log_exist(os.path.join(cfg["logger_save_dir"], cfg["logger_name"]), cfg["logger_version"])

    # Define callbacks
    logger = TensorBoardLogger(
        save_dir=cfg["logger_save_dir"],
        name=cfg["logger_name"],
        version=name_logger_version
    )
    # log hparams into Tensorboard
    logger.log_hyperparams(cfg)

    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        filename=cfg["logger_name"] + "_" + name_logger_version + "_{epoch}",
        dirpath=cfg["ckpt_dirpath"],
        monitor=cfg["ckpt_monitor"] ,
        mode=cfg["ckpt_mode"],
        save_top_k=cfg["ckpt_save_top_k"],
        save_weights_only=cfg["ckpt_save_weights_only"]
    )

    lightning_module = DETRObjectDetector(
        processor=image_processor,
        model=model,
        learning_rate=cfg["learning_rate"],
        weight_decay=cfg["weight_decay"],
        nb_epochs=cfg["nb_epochs"],
    )

    trainer = pl.Trainer(
        log_every_n_steps=10,
        accelerator=cfg["accelerator"],
        devices=cfg["devices"],
        logger=logger,
        callbacks=[checkpoint_callback],
        max_epochs=cfg["nb_epochs"],
        num_sanity_val_steps=0,
    )

    # ######## RUN TRAINING ########
    trainer.fit(lightning_module, datamodule=datamodule)
    print(f"[INFO]... best_model_path: {checkpoint_callback.best_model_path}")
    # trainer.test(lightning_module, datamodule=datamodule, ckpt_path=checkpoint_callback.best_model_path)


if __name__ == "__main__":
    config_path = "configs\config_training_detr_table_detector.yaml"

    # Read config
    with open(config_path, "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    train_trocr(cfg)