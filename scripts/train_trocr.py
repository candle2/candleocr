from typing import Dict, Any

import yaml
import os
import pandas as pd
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from transformers import TrOCRProcessor, VisionEncoderDecoderModel

from candleocr.utils.utils import check_log_exist
from candleocr.datasets import TrOCRDataset
from candleocr.dataloaders import TrOCRDataModule
from candleocr.lightning_module import TrOCRlightningModule

import torch
torch.cuda.empty_cache()


def train_trocr(cfg: Dict[str, Any]):
    
    processor = TrOCRProcessor.from_pretrained("microsoft/trocr-base-stage1")
  
    train_dataset = TrOCRDataset(
        root_dir="D:\PycharmProjects\candle\candleocr\data\words_cropped",
        processor=processor,
        path_data=r"D:\PycharmProjects\candle\candleocr\data\train_dataset_trocr.pkl"
    )
    val_dataset = TrOCRDataset(
        root_dir="D:\PycharmProjects\candle\candleocr\data\words_cropped",
        processor=processor,
        path_data=r"D:\PycharmProjects\candle\candleocr\data\val_dataset_trocr.pkl"
    )

    # Data loader
    datamodule = TrOCRDataModule(
            train_dataset=train_dataset,
            val_dataset=val_dataset,
            num_workers=cfg["num_workers"],
            batch_size=cfg["batch_size"],
            drop_last=True,
        )

    #Load Model
    model = VisionEncoderDecoderModel.from_pretrained("microsoft/trocr-base-stage1")
    # set special tokens used for creating the decoder_input_ids from the labels
    model.config.decoder_start_token_id = processor.tokenizer.cls_token_id
    model.config.pad_token_id = processor.tokenizer.pad_token_id
    # make sure vocab size is set correctly
    model.config.vocab_size = model.config.decoder.vocab_size

     # set beam search parameters
    model.config.eos_token_id = processor.tokenizer.sep_token_id
    model.config.max_length = 32
    model.config.early_stopping = True
    model.config.no_repeat_ngram_size = 3
    model.config.length_penalty = 2.0
    model.config.num_beams = 4

    name_logger_version = check_log_exist(os.path.join(cfg["logger_save_dir"], cfg["logger_name"]), cfg["logger_version"])

    # Define callbacks
    logger = TensorBoardLogger(
        save_dir=cfg["logger_save_dir"],
        name=cfg["logger_name"],
        version=name_logger_version
    )
    # log hparams into Tensorboard
    logger.log_hyperparams(cfg)

    checkpoint_callback = pl.callbacks.ModelCheckpoint(
        filename=cfg["logger_name"] + "_" + name_logger_version + "_{epoch}",
        dirpath=cfg["ckpt_dirpath"],
        monitor=cfg["ckpt_monitor"] ,
        mode=cfg["ckpt_mode"],
        save_top_k=cfg["ckpt_save_top_k"],
        save_weights_only=cfg["ckpt_save_weights_only"]
    )

    lightning_module = TrOCRlightningModule(
        model=model,
        processor=processor,
        learning_rate=cfg["learning_rate"],
        weight_decay=cfg["weight_decay"],
        batch_size=cfg["batch_size"],
        nb_epochs=cfg["nb_epochs"],
    )

    trainer = pl.Trainer(
        log_every_n_steps=10,
        accelerator=cfg["accelerator"],
        devices=cfg["devices"],
        logger=logger,
        callbacks=[checkpoint_callback],
        max_epochs=cfg["nb_epochs"],
        num_sanity_val_steps=0,
    )

    # ######## RUN TRAINING ########
    trainer.fit(lightning_module, datamodule=datamodule)
    print(f"[INFO]... best_model_path: {checkpoint_callback.best_model_path}")
    # trainer.test(lightning_module, datamodule=datamodule, ckpt_path=checkpoint_callback.best_model_path)


if __name__ == "__main__":
    config_path = "configs\config_training_trocr.yaml"

    # Read config
    with open(config_path, "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    train_trocr(cfg)